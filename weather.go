package main

import (
	"fmt"

	"github.com/alsm/forecastio"
	"gitlab.com/davidjpeacock/shelbot-ii/slack"
)

type Weather struct {
	airports Airports
	apiKey   string
}

func (w *Weather) lookup(a *slack.ActionData) {
	location := a.Params["loc"]
	if w.apiKey == "" {
		// Do not add key; it goes in main.go with other flag defaults
		// No api key, no weather - forecastio.io for key
		return
	}

	ap := w.airports.Lookup(location)
	if ap == nil {
		a.Reply(fmt.Sprintf("Sorry, I couldn't find an airport with the code %s", location))
		return
	}

	c := forecastio.NewConnection(w.apiKey)
	c.SetUnits("si")
	f, err := c.Forecast(ap.Latitude, ap.Longitude, nil, false)
	if err != nil || f == nil {
		a.Reply(fmt.Sprintf("Sorry, there was an error looking up the weather for %s", ap.Name))
		return
	}

	response := fmt.Sprintf("The weather at %s is %s and %.1fC", ap.Name, f.Currently.Summary, f.Currently.Temperature)
	a.Reply(response)
	a.Log(response)
}
