package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/user"
	"path/filepath"
	"regexp"

	"gitlab.com/davidjpeacock/shelbot-ii/slack"
)

func main() {
	var err error
	var logFile *os.File

	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	homeDir := usr.HomeDir

	karmaFile := flag.String("karmaFile", filepath.Join(homeDir, ".shelbot.json"), "karma db file")
	slackToken := flag.String("slackToken", "", "slackbot API Token")
	debug := flag.Bool("debug", false, "Enable debug (print log to screen)")
	airportFile := flag.String("airportFile", filepath.Join(homeDir, "airports.csv"), "airport data csv file")
	apiKey := flag.String("forecastioKey", "", "Forcast.io API key")
	flag.Parse()

	if *slackToken == "" {
		flag.Usage()
		log.Fatalln("slackToken cannot be empty")
	}

	logger := log.New(os.Stdout, "SLACK: ", log.LstdFlags)
	if !*debug {
		logFile, err = os.OpenFile(filepath.Join(homeDir, ".shelbot.log"), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
		if err != nil {
			log.Fatal(err)
		}
		log.SetOutput(logFile)
		logger.SetOutput(logFile)
		defer logFile.Close()
	}

	k, err := readKarmaFileJSON(*karmaFile)
	if err != nil {
		log.Fatalln(err)
	}

	weather := &Weather{apiKey: *apiKey}
	if weather.airports, err = LoadAirports(*airportFile); err != nil {
		log.Fatalln(err)
	}

	bot := slack.NewBot(*slackToken)
	bot.Logger = logger

	bot.Command(regexp.MustCompile(`(?P<subject>\w+)\+\+`), func(a *slack.ActionData) {
		sub := a.Params["subject"]
		k.increment(sub)
		a.Reply(fmt.Sprintf("Karma for %s is now %d", sub, k.query(sub)))
		k.save()
	}, false)

	bot.Command(regexp.MustCompile(`(?P<subject>\w+)\-\-`), func(a *slack.ActionData) {
		sub := a.Params["subject"]
		k.decrement(sub)
		a.Reply(fmt.Sprintf("Karma for %s is now %d", sub, k.query(sub)))
		k.save()
	}, false)

	bot.Command(regexp.MustCompile(`(?i)^weather (?P<loc>\S+)`), weather.lookup, true)

	bot.Command(regexp.MustCompile(`(?i)^wiki (?P<wiki>.+)`), wikiLookup, true)

	bot.Command(regexp.MustCompile(`(?i)^help$`), getQuote, true)

	bot.Command(regexp.MustCompile(`(?i)^tell (?P<user>\S+) (?P<channel>\S+) (?P<message>.+)`), func(a *slack.ActionData) {
		msg := fmt.Sprintf("<@%s> %s", a.Bot.UserByName(a.Params["user"]).ID, a.Params["message"])
		fmt.Println(msg)
		bot.Message(a.Bot.ChannelByName(a.Params["channel"]), msg)
	}, false)

	bot.Go()
}
