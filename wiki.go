package main

import (
	"encoding/json"
	"fmt"
	"html"
	"net/http"
	"strings"
	"time"

	"gitlab.com/davidjpeacock/shelbot-ii/slack"
)

type WikiLookup struct {
	Batchcomplete string `json:"batchcomplete"`
	Query         struct {
		Pages map[string]struct {
			Pageid               int       `json:"pageid"`
			Ns                   int       `json:"ns"`
			Title                string    `json:"title"`
			Extract              string    `json:"extract"`
			Contentmodel         string    `json:"contentmodel"`
			Pagelanguage         string    `json:"pagelanguage"`
			Pagelanguagehtmlcode string    `json:"pagelanguagehtmlcode"`
			Pagelanguagedir      string    `json:"pagelanguagedir"`
			Touched              time.Time `json:"touched"`
			Lastrevid            int       `json:"lastrevid"`
			Length               int       `json:"length"`
			Fullurl              string    `json:"fullurl"`
			Editurl              string    `json:"editurl"`
			Canonicalurl         string    `json:"canonicalurl"`
		} `json:"pages"`
	} `json:"query"`
}

func wikiLookup(a *slack.ActionData) {
	var w WikiLookup
	term := a.Params["wiki"]

	resp, err := http.Get("https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts|info&redirects&exintro=&inprop=url&explaintext=&titles=" + html.EscapeString(strings.Replace(term, " ", "%20", -1)))
	if err != nil || resp.StatusCode != 200 {
		a.Reply(fmt.Sprintf("Sorry, there was an error looking up a wiki article on %s", term))
		a.Log(fmt.Sprintf("could not send message:", err))
		return
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&w); err != nil {
		a.Reply(fmt.Sprintf("Sorry, there was an error looking up a wiki article on %s", term))
		a.Log(fmt.Sprintf("could not send message:", err))
		return
	}
	for page, entry := range w.Query.Pages {
		if page == "-1" {
			a.Reply(fmt.Sprintf("Sorry, there was no article for '%s'", term))
			return
		}
		a.Reply(strings.Split(entry.Extract, "\n")[0])
		a.Reply(entry.Fullurl)
		a.Log("Wikipedia extract provided:", entry.Fullurl)
	}
}
