package slack

import (
	"fmt"
	"log"
	"regexp"
	"strings"

	"github.com/nlopes/slack"
)

type Bot struct {
	API            *slack.Client
	RTMAPI         *slack.RTM
	directCommands []*command
	commands       []*command
	info           *slack.Info
	id             string
	Logger         *log.Logger
}

func NewBot(token string) *Bot {
	var bot Bot

	bot.API = slack.New(token)
	bot.RTMAPI = bot.API.NewRTM()

	return &bot
}

func (b *Bot) Go() error {
	go b.RTMAPI.ManageConnection()

	for msg := range b.RTMAPI.IncomingEvents {
		switch ev := msg.Data.(type) {
		case *slack.ConnectedEvent:
			b.info = b.RTMAPI.GetInfo()
			b.id = ev.Info.User.ID
		case *slack.MessageEvent:
			if b.isMyself(ev) {
				continue
			}
			for _, c := range b.commands {
				if c.match(ev) {
					b.Logger.Printf("%+v\n", ev.Msg)
					c.f(&ActionData{
						MessageEvent: *ev,
						Bot:          b,
						Params:       c.getParams(ev),
					})
				}
			}
			if b.toMe(ev) {
				b.Logger.Printf("%+v\n", ev.Msg)
				for _, c := range b.directCommands {
					if c.match(ev) {
						c.f(&ActionData{
							MessageEvent: *ev,
							Bot:          b,
							Params:       c.getParams(ev),
						})
					}
				}
			}
		}
	}

	return nil
}

func (b *Bot) Command(r *regexp.Regexp, a Action, directed bool) {
	if directed {
		b.directCommands = append(b.directCommands, &command{
			r: r,
			f: a,
		})
	} else {
		b.commands = append(b.commands, &command{
			r: r,
			f: a,
		})
	}
}

func (b *Bot) isMyself(m *slack.MessageEvent) bool {
	return m.User == b.info.User.ID
}

func (b *Bot) toMe(m *slack.MessageEvent) bool {
	prefix := fmt.Sprintf("<@%s>", b.id)
	if strings.HasPrefix(m.Msg.Text, prefix) {
		m.Msg.Text = strings.TrimSpace(strings.TrimPrefix(m.Msg.Text, prefix))
		return true
	}

	return false
}

func (b *Bot) Log(s ...string) {
	b.Logger.Println(s)
}

func (b *Bot) ChannelByName(name string) *slack.Channel {
	for _, ch := range b.RTMAPI.GetInfo().Channels {
		if ch.Name == name {
			return &ch
		}
	}
	return nil
}

func (b *Bot) UserByName(name string) *slack.User {
	for _, u := range b.RTMAPI.GetInfo().Users {
		if u.Name == name {
			return &u
		}
	}
	return nil
}

func (b *Bot) Message(channel *slack.Channel, msg string) {
	b.API.PostMessage(channel.ID, msg, slack.PostMessageParameters{})
}
